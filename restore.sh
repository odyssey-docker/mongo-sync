#!/bin/bash

: ${MONGODB_RESTORE_HOST:=mongodb};
: ${MONGODB_RESTORE_PORT:=27017};

# if no username given but a password was given default username to admin
[[ ( -z "${MONGODB_RESTORE_USER}" ) && ( -n "${MONGODB_RESTORE_PASS}" ) ]] && MONGODB_RESTORE_USER='admin'

# only add options if there are values to use with them
[[ ( -n "${MONGODB_RESTORE_USER}" ) ]] && USER_RESTORE_STR=" --username ${MONGODB_RESTORE_USER}"}
[[ ( -n "${MONGODB_RESTORE_PASS}" ) ]] && PASS_RESTORE_STR=" --password ${MONGODB_RESTORE_PASS}"
[[ ( -n "${MONGODB_RESTORE_DB}" ) ]] && DB_RESTORE_STR=" --db ${MONGODB_RESTORE_DB}"

# should be full filename with extension and no directory paths e.g. 2015.08.06.171901.tgz
FILE_TO_RESTORE=${1}

# we we are given a specific file name we grab it from s3 if it exists there first, allowing us to restore on different servers
if [ -n ${FILE_TO_RESTORE} ]; then
    if [ ! -s /backup/${FILE_TO_RESTORE} ]; then
        echo "=> Downloading ${FILE_TO_RESTORE} from S3"
        aws s3 cp s3://$S3_BUCKET/$S3_PATH/${FILE_TO_RESTORE} "/backup/${FILE_TO_RESTORE}"
    fi
fi

# if no file is given we simply look for the most recent backup to restore
[[ -z "${1}" ]] && FILE_TO_RESTORE=$(ls /backup -N1 | grep -iv ".tgz" | sort -r | head -n 1)

# @TODO add method to grab latest backup from s3 if nothing there is one newer than on the server or if the server file doesnt exist.

echo "=> Restore database from ${FILE_TO_RESTORE}"
if mongorestore --host ${MONGODB_RESTORE_HOST} --port ${MONGODB_RESTORE_PORT} ${USER_RESTORE_STR}${PASS_RESTORE_STR}${DB_RESTORE_STR} /backup/${FILE_TO_RESTORE}; then
    echo "   Restore succeeded"
else
    echo "   Restore failed"
fi
echo "=> Done"
