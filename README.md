# mongo-sync

    This image extends the excellent [tutumcloud/monogodb-backup](https://github.com/tutumcloud/mongodb-backup) project.

This image syncs data between two running mongodb databases by running mongodump on the backup database and importing it to the restore mongodb database. Intermediate database dumps are saved to `/backup` and can optionally be backed up to Amazon S3.

This image may also be used for individual backup and/or restore functionality.

## Usage for backup and restore from db to another:

```
docker run -d \
    --env MONGODB_BACKUP_HOST=mongodb.backup.host \
    --env MONGODB_BACKUP_PORT=27017 \
    --env MONGODB_BACKUP_USER=admin \
    --env MONGODB_BACKUP_PASS=password \
    --env MONGODB_RESTORE_HOST=mongodb.restore.host \
    --env MONGODB_RESTORE_PORT=27017 \
    --env MONGODB_RESTORE_USER=admin \
    --env MONGODB_RESTORE_PASS=password \
    --env AWS_ACCESS_KEY_ID=changeme \
    --env AWS_SECRET_ACCESS_KEY=changeme \
    --env AWS_DEFAULT_REGION=ap-southeast-2 \
    --env S3_BUCKET=changeme \
    --env S3_PATH=mongodb \
    --env S3_BACKUP=yes \
    --volume host.folder:/backup \
    --name mongodb-sync \
    registry.gitlab.com/odyssey-docker/mongo-sync:latest
```


## Parameters

    ## Optional Backup Settings
    MONGODB_BACKUP_HOST    the host/ip/compose service name of the mongodb database you wish to backup `defaults to mongodb`
    MONGODB_BACKUP_PORT    the port number of the mongodb database you wish to backup `defaults to 27017`
    MONGODB_BACKUP_USER    the username of the mongodb database you wish to backup. If MONGODB_BACKUP_USER is empty while MONGODB_BACKUP_PASS is not `defaults to admin`
    MONGODB_BACKUP_PASS    the password of the mongodb database you wish to backup
    MONGODB_BACKUP_DB      the database name to dump. If not specified, it will dump all the databases
    EXTRA_BACKUP_OPTS      the extra options to pass to mongodump command

    ## Optional Restore Settings
    MONGODB_RESTORE_HOST    the host/ip/compose service name of the mongodb database you wish to restore `defaults to mongodb`
    MONGODB_RESTORE_PORT    the port number of the mongodb database you wish to restore `defaults to 27017`
    MONGODB_RESTORE_USER    the username of the mongodb database you wish to restore. If MONGODB_RESTORE_USER is empty while MONGODB_RESTORE_PASS is not `defaults to admin`
    MONGODB_RESTORE_PASS    the password of the mongodb database you wish to restore
    MONGODB_RESTORE_DB      the database name to restore. If not specified, it will restore all the databases
    EXTRA_RESTORE_OPTS      the extra options to pass to mongorestore command

    ## AWS S3 options
    S3_BACKUP               If set, backups will be archived to S3. some of the following options will be required if this is set
    S3_BUCKET               The name of the bucket where the backup will be copied.
    S3_PATH                 The path within the bucket where the database dump archive will be saved
    
    ## AWS S3 optional settings `NOTE IAM profiles and users are passed into the continaer`
    AWS_ACCESS_KEY_ID       The AWS access key for the account to which the backup will be made. `Not required if IAM profiles are set for ec2 instance`
    AWS_SECRET_ACCESS_KEY   The AWS secret key for the account to which the backup will be made. `Not required if IAM profiles are set for ec2 instance`
    AWS_DEFAULT_REGION      The default region for the backup bucket. `defaults to ap-southeast-2`
    CRON_TIME               The interval of cron job to run mongodump. `0 0 * * *` by default, which is every day at 00:00
    MAX_BACKUPS             The number of backups to keep. When reaching the limit, the old backup will be discarded. No limit, by default. **Note: s3 backups will not be purged in this process. Select an expiration date in your bucket to enforce cloud backups.**

    INIT_BACKUP             If set, create a backup when the container launched. `NOTE only works if the command is overridden for the container to backup`
    INIT_RESTORE            If set, restore the most current backup when the container launched. `NOTE only works if the command is overridden for the container to restore`
    INIT_SYNC               If set, sync the two mongodb databases immediately when the container launched. `Container command defaults to sync so this will always run if neither of the above are set`

## Run exclusively as a backup process

To run this image only as a backup process:

```
docker run -d \
    --env MONGODB_BACKUP_HOST=mongodb.backup.host \
    --env MONGODB_BACKUP_PORT=27017 \
    --env MONGODB_BACKUP_USER=admin \
    --env MONGODB_BACKUP_PASS=password \
    --volume host.folder:/backup \
    --name mongodb-sync \
    registry.gitlab.com/odyssey-docker/mongo-sync:latest backup
```


To archive copies of the the backups to S3:

```
docker run -d \
    --env MONGODB_BACKUP_HOST=mongodb.backup.host \
    --env MONGODB_BACKUP_PORT=27017 \
    --env MONGODB_BACKUP_USER=admin \
    --env MONGODB_BACKUP_PASS=password \
    --env AWS_ACCESS_KEY_ID=changeme \
    --env AWS_SECRET_ACCESS_KEY=changeme \
    --env AWS_DEFAULT_REGION=ap-southeast-2 \
    --env S3_BUCKET=changeme \
    --env S3_PATH=mongodb \
    --env S3_BACKUP=yes \
    --volume host.folder:/backup \
    --name mongo-sync \
    registry.gitlab.com/odyssey-docker/mongo-sync:latest backup
```

To archive copies of the the backups to S3 with docker compose

```
  mongo-sync:
      image: registry.gitlab.com/odyssey-docker/mongo-sync:latest
      command: backup
      environment:        
        - reschedule:on-node-failure
        - MONGODB_BACKUP_HOST=mongo
        - MONGODB_BACKUP_PORT=27017
        - S3_BUCKET=${AWS_S3_BACKUP_BUCKET}
        - S3_PATH=${AWS_S3_BACKUP_PATH}
        - S3_BACKUP=yes
        - MAX_BACKUPS=10
        - INIT_BACKUP=yes
      restart: always
      volumes:
        - host.folder:/backup
```

## Restore from a backup

To see the list of backups in a running backup container, you can run:

```
docker exec mongo-sync ls /backup
```

To restore a mongodb database from an existing backup on disk

```
docker run -it --rm \
    --env MONGODB_RESTORE_HOST=mongodb.restore.host \
    --env MONGODB_RESTORE_PORT=27017 \
    --env MONGODB_RESTORE_USER=admin \
    --env MONGODB_RESTORE_PASS=password \
    --volume /existing/local/backup/folder:/backup \
    registry.gitlab.com/odyssey-docker/mongo-sync:latest /restore.sh 2015.08.06.171901.tgz
```

## Run as a one-off sync process

If you have need to run one-off sync processes such as creating snapshots of your production db for testing in a QA environment, you can invoke this image as needed using the following command.

```
docker run -d --rm \
    --env MONGODB_BACKUP_HOST=mongodb.backup.host \
    --env MONGODB_BACKUP_PORT=27017 \
    --env MONGODB_BACKUP_USER=admin \
    --env MONGODB_BACKUP_PASS=password \
    --env MONGODB_RESTORE_HOST=mongodb.restore.host \
    --env MONGODB_RESTORE_PORT=27017 \
    --env MONGODB_RESTORE_USER=admin \
    --env MONGODB_RESTORE_PASS=password \
    registry.gitlab.com/odyssey-docker/mongo-sync:latest /sync.sh
```
